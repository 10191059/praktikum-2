package com.muhammadyusfadillah_10191059.praktikum2.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.app.FragmentBreadCrumbs;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.muhammadyusfadillah_10191059.praktikum2.R;
import com.muhammadyusfadillah_10191059.praktikum2.fragment.FirstFragment;
import com.muhammadyusfadillah_10191059.praktikum2.fragment.SecondFragment;

public class SiklusHidupActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_siklus_hidup);

        Button fragment1=findViewById(R.id.fragment1);
        Button fragment2=findViewById(R.id.fragment2);

        fragment1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FragmentManager fm=getSupportFragmentManager();
                FragmentTransaction ft=fm.beginTransaction();
                ft.replace(R.id.relativeLayout,new FirstFragment());
                ft.commit();

            }
        });

        fragment2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FragmentManager fm=getSupportFragmentManager();
                FragmentTransaction ft=fm.beginTransaction();
                ft.replace(R.id.relativeLayout,new SecondFragment());
                ft.commit();

            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        Toast.makeText(this, "Metode Dimulai", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Toast.makeText(this, "Metode Selesai", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        Toast.makeText(this, "Metode Ditunda", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Toast.makeText(this, "Metode Ditunda", Toast.LENGTH_SHORT).show();
    }
}