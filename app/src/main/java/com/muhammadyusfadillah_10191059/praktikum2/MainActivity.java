package com.muhammadyusfadillah_10191059.praktikum2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.muhammadyusfadillah_10191059.praktikum2.activity.SiklusHidupActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btn_siklushidup=findViewById(R.id.btn_siklushidup);

        btn_siklushidup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MainActivity.this, SiklusHidupActivity.class);
                startActivity(intent);
            }
        });
    }
}